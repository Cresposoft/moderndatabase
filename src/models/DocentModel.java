package models;

import com.cresposoft.moderndatabase.ModernDataBase;

import java.util.ArrayList;

/**
 * Created by ESTUDIANTE on 17/03/14.
 */
public class DocentModel extends ModernDataBase {
    public static final String TABLE = "docents";
    public static final String PK = "cc";
    
    public DocentModel(){
        super(Docent.class, "docents");
    }

}
