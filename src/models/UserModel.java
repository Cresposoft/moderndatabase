package models;

import com.cresposoft.moderndatabase.ModernDataBase;

import java.util.ArrayList;

/**
 * Cresposoft Technology Created by Miguel on 17/03/14.
 */
public class UserModel extends ModernDataBase {

    public static final String TABLE = "users";
    public static final String PK = "code";

    public UserModel() {
        super(User.class, "users");
    }
}
