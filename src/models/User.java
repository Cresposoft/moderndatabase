package models;
/**
 * Cresposoft Technology
 * Created by Miguel on 17/03/14.
 */
public class User {
    private int id;
    private int code;
    private String name;
    private String email;
    public User(){

    }
    public User(int code, String name, String email){
        this.code = code;
        this.name = name;
        this.email = email;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }
}
