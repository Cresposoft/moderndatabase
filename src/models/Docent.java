package models;

/**
 * Created by ESTUDIANTE on 17/03/14.
 */
public class Docent {

    private int id;
    private int cc;
    private String name;
    private String email;
    public Docent(){

    }
    public Docent(int cc, String name, String email) {
        this.cc = cc;
        this.name = name;
        this.email = email;

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the cc
     */
    public int getCc() {
        return cc;
    }

    /**
     * @param cc the cc to set
     */
    public void setCc(int cc) {
        this.cc = cc;
    }
}
