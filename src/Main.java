import models.Docent;
import models.DocentModel;
import models.User;
import models.UserModel;

import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by ESTUDIANTE on 17/03/14.
 */
public class Main {
    public static void main(String[] args){
        UserModel usermodel = new UserModel();
        ArrayList<User> users = usermodel.getRows("");
        for(User user2 : users){
            System.out.println("ID: "+user2.getId());
            System.out.println("Code: "+user2.getCode());
            System.out.println("Name: "+user2.getName());
            System.out.println("Email: "+user2.getEmail());
            System.out.println("------------------");
        }
        System.out.println("Docentes");
        System.out.println("---------------");
        DocentModel docentmodel = new DocentModel();
        ArrayList<Docent> docents = docentmodel.getRows("");
        for(Docent docent2 : docents){
            System.out.println("ID: "+docent2.getId());
            System.out.println("CC: "+docent2.getCc());
            System.out.println("Name: "+docent2.getName());
            System.out.println("Email: "+docent2.getEmail());
            System.out.println("------------------");
        }
    }

}
